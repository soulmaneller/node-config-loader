const { expect } = require( 'chai' );
const fs    = require( 'fs-extra' );
const path  = require( 'path' );

const config = require( '../' );
const defaultData = {
    foo: 'bar',
    num: 100,
    obj: {
        a: 1,
        b: 2
    }
}
const defaultFailData = '{ a: 1, b: 2';
const defaultFilepath = path.join( 'config', 'config.json' );
describe( `Config loader`, () => {
    beforeEach( cleanDir );
    afterEach( cleanDir );

    describe( `Creating config file`, () => {
        describe( `Default settings`, () => {
            it( `Should generate a config file`, () => {
                config();
                expect( fs.existsSync( defaultFilepath )).to.be.true;
            });

            it( `Should generate a config file with default configuration`, () => {
                config( defaultData );
                let configData = fs.readJsonSync( defaultFilepath );
                expect( configData ).to.deep.equal( defaultData );
            });
        });

        describe( `Setting by options`, () => {
            const options = {
                CONFIG_DIR: 'sample_config',
                CONFIG_FILE: 'sample-config.json'
            }

            const filepath = path.join( 'sample_config', 'sample-config.json' );
            it( `Should generate a config file`, () => {
                config( {}, options );
                expect( fs.existsSync( filepath )).to.be.true;
            });

            it( `Should generate a config file with default configuration`, () => {
                config( defaultData, options );
                let configData = fs.readJsonSync( filepath );
                expect( configData ).to.deep.equal( defaultData );
            });
        });

        describe( `Setting by environment variables`, () => {
            before(() => {
                process.env.CONFIG_DIR = 'sample_config';
                process.env.CONFIG_FILE = 'sample-config.json';
            });
            after(() => {
                delete process.env.CONFIG_DIR;
                delete process.env.CONFIG_FILE;
            });
            const filepath = path.join( 'sample_config', 'sample-config.json' );
            it( `Should generate a config file`, () => {
                config();
                expect( fs.existsSync( filepath )).to.be.true;
            });

            it( `Should generate a config file with default configuration`, () => {
                config( defaultData );
                let configData = fs.readJsonSync( filepath );
                expect( configData ).to.deep.equal( defaultData );
            });
        });
    });

    it( `Should generate a config file with default configuration and backup old config file`, () => {
        fs.outputFileSync( defaultFilepath, defaultFailData );
        config( defaultData );
        let configData = fs.readJsonSync( defaultFilepath );
        expect( configData ).to.deep.equal( defaultData );

        let filelist = fs.readdirSync( 'config' );
        expect( filelist ).have.length( 2 );
    });

    it( `Should not overwrite config file`, () => {
        fs.outputFileSync( defaultFilepath, defaultFailData );
        config( defaultData, { CONFIG_OVERWRITE: false });
        let configData = fs.readFileSync( defaultFilepath, { encoding: 'utf8' });
        expect( configData ).to.equal( defaultFailData );

        let filelist = fs.readdirSync( 'config' );
        expect( filelist ).have.length( 1 );
    });
});

function cleanDir() {
    const list = [ 'config', 'sample_config' ];
    list.forEach(( name ) => {
        fs.removeSync( name );
    });
}

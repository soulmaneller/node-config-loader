const fs        = require( 'fs-extra' );
const path      = require( 'path' );
const _         = require( 'lodash' );
const moment    = require( 'moment' );
const json5     = require( 'json5' );

module.exports = ( DEFAULT_CONFIG = {}, options = {} ) => {
    const env               = _.get.bind( null, process.env );
    const CONFIG_DIR        = options.CONFIG_DIR || env( 'CONFIG_DIR' ) || 'config';
    const CONFIG_FILE       = options.CONFIG_FILE || env( 'CONFIG_FILE' ) || 'config.json';
    const CONFIG_OVERWRITE  = getValue( 'CONFIG_OVERWRITE' );
    const CONFIG_MERGE      = getValue( 'CONFIG_MERGE' );
    const postProcess       = typeof options.postProcess === 'function' ? options.postProcess : null;
    const configPath        = path.join( CONFIG_DIR, CONFIG_FILE );

    let config = _.cloneDeep( DEFAULT_CONFIG );
    const isExists = fs.existsSync( configPath );
    if( isExists ) {
        try {
            config = json5.parse( fs.readFileSync( configPath, { encoding: 'utf8' }));
        } catch (e) {
            if( CONFIG_OVERWRITE ) {
                let backupFile = CONFIG_FILE + `_${ moment().format( 'YYYYMMDD_HHmmss' ) }.bak`;
                let backupConfigPath = path.join( CONFIG_DIR, backupFile );
                fs.copySync( configPath, backupConfigPath );
                console.warn( `Please check configuration file (${ configPath })` );
            }
        }

        if( CONFIG_MERGE ) {
            config = _.merge( DEFAULT_CONFIG, config );
        }
    }

    if( CONFIG_OVERWRITE || !isExists ) {
        fs.outputJsonSync( configPath, config, { spaces: 4 });
    }

    if( postProcess !== null ) {
        postProcess( config );
    }

    return _.get.bind( this, config );

    function getValue( name, defaultValue = true ) {
        if( options.hasOwnProperty( name )) return !!options[ name ];
        return !!env( name, defaultValue );
    }
};
